#repl

javascript的在线repl [在线演示](http://repl.diqye.com/#bGV0IHN1bSA9IHhzID0+IHhzLmxlbmd0aCA9PSAwPzA6eHNbMF0rc3VtKHhzLnNsaWNlKDEpKQpzdW0oWzEwLC0yLDMwLDQwXSkK)

## 特点
1. 实时反馈结果，特别适合写小算法，小函数。
2. url自带代码分享， 可发送给好友帮忙查看
3. 智能代码编辑器
4. 语法高亮
6. 作用域隔离 （解决const,let报错等问题）

## 使用方法

1. 默认编辑器中最后一行（非空白行）的结果打印出来，
2. 可以使用内置log 函数 打印其它结果
3. 最后一行会自动加上return 关键字


## 群

QQ : 632826899
